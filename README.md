# Lidgren.Network

Fork of Lidgren.Network to be used in GTAServer.core

## Fork changes

* Support for Q3-compatible OOB messages
* PacketSent, PacketReceived events
* USE_RELEASE_STATISTICS enabled by default
